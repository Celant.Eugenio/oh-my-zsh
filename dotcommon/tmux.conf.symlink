##############
## BINDINGS ##
##############

# Unbind unused key bindings
unbind C-b

# Use CTRL-A as screen
set -g prefix C-a
bind C-a send-prefix

# Kill window with CTRL-A-W
bind-key w kill-pane

# Don't rename windows automatically
set-option -g allow-rename off

# Rename window with CTRL-A-R
bind-key r command-prompt 'rename-window %%'

# Window split
bind-key - split-window -v
bind-key | split-window -h

# Join two pane with vertical split
# then you can use prefix 'space' to change organization
bind-key j choose-window "join-pane -h -s '%%'"

# CTRL-SHIFT-Left move window to the left
bind-key -n C-S-Left swap-window -t -1
# CTRL-SHIFT-Right move window to the right
bind-key -n C-S-Right swap-window -t +1

#############
## GENERAL ##
#############

# More terminal configuration
set -g default-terminal $ZSH_TMUX_TERM
set -g terminal-overrides "xterm*:XT:smcup@:rmcup@"

# Enable mouse
setw -g mode-mouse on
set -g mouse-utf8 off
set -g mouse-select-window on
set -g mouse-select-pane on
set -g mouse-resize-pane on

# Start numbering at 1
set -g base-index 1
setw -g pane-base-index 1
set -g renumber-windows on

# Use 256 colors
set -g default-terminal "screen-256color"

############
## DESIGN ##
############

## COLORSCHEME: gruvbox dark
set-option -g status "on"

# Default statusbar colors
set-option -g status-bg colour237 #bg1
set-option -g status-fg colour223 #fg1

# Default window title colors
set-window-option -g window-status-bg colour214 #yellow
set-window-option -g window-status-fg colour237 #bg1

set-window-option -g window-status-activity-bg colour237 #bg1
set-window-option -g window-status-activity-fg colour248 #fg3

# Active window title colors
set-window-option -g window-status-current-bg default
set-window-option -g window-status-current-fg colour237 #bg1

# Pane border
set-option -g pane-active-border-fg colour250 #fg2
set-option -g pane-border-fg colour237 #bg1

# Message infos
set-option -g message-bg colour239 #bg2
set-option -g message-fg colour223 #fg1

# Writting commands inactive
set-option -g message-command-bg colour239 #fg3
set-option -g message-command-fg colour223 #bg1

# Pane number display
set-option -g display-panes-active-colour colour250 #fg2
set-option -g display-panes-colour colour237 #bg1

# Clock
set-window-option -g clock-mode-colour colour109 #blue

# Bell
## Visual bell is the text bell
set-option -g visual-bell off
#set-window-option -g window-status-bell-bg colour167
#set-window-option -g window-status-bell-fg colour235

## Theme settings mixed with colors (unfortunately, but there is no cleaner way)
set-option -g status-attr "none"
set-option -g status-justify "left"
set-option -g status-left-attr "none"
set-option -g status-left-length "80"
set-option -g status-right-attr "none"
set-option -g status-right-length "80"
set-window-option -g window-status-activity-attr "none"
set-window-option -g window-status-attr "none"
set-window-option -g window-status-separator ""

set-option -g status-left "#[fg=colour248, bg=colour241] #S #[fg=colour241, bg=colour237, nobold, noitalics, nounderscore]"
set-option -g status-right "#[fg=colour239, bg=colour237, nobold, nounderscore, noitalics]#[fg=colour246,bg=colour239] %d-%m-%Y  %H:%M #[fg=colour248, bg=colour239, nobold, noitalics, nounderscore]#[fg=colour237, bg=colour248] #h "

set-window-option -g window-status-current-format "#[fg=colour239, bg=colour248, :nobold, noitalics, nounderscore]#[fg=colour239, bg=colour214] #I #[fg=colour239, bg=colour214, bold] #W #[fg=colour214, bg=colour237, nobold, noitalics, nounderscore]"
set-window-option -g window-status-format "#[fg=colour237,bg=colour239,noitalics]#[fg=colour223,bg=colour239] #I #[fg=colour223, bg=colour239] #W #[fg=colour239, bg=colour237, noitalics]"
