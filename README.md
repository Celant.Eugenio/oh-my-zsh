<p align="center">
  <img src="https://s3.amazonaws.com/ohmyzsh/oh-my-zsh-logo.png" alt="Oh My Zsh">
</p>

Oh My Zsh is an open source, community-driven framework for managing your [zsh](http://www.zsh.org/) configuration.

You can find more information about the project in the [official repository](https://github.com/robbyrussell/oh-my-zsh).

# Introduction

This is a fork from the original project and has been modified to suit my personal needs.

The main differences you will find in this repository are:
- new theme: **eugenio**: This theme is a mix of bira, gnzh and agnoster. It also adds some steps to the installation script like program downloads and font downloads.
- a lot of new aliases, mainly for git and common commands
- [lsd](https://github.com/Peltoche/lsd)
- [dutree](https://github.com/nachoparker/dutree)
- custom tmux configuration

# Installation

```
user@hostname:~$ git clone git@gitlab.com:Celant.Eugenio/oh-my-zsh.git
user@hostname:~$ mv oh-my-zsh .oh-my-zsh
user@hostname:~$ sudo apt install wget
user@hostname:~$ sudo apt install unzip
user@hostname:~$ sudo apt install fontconfig
user@hostname:~/.oh-my-zsh$ cd .oh-my-zsh
user@hostname:~/.oh-my-zsh$ ./script/bootstrap
```

The installation will install all needed dotfiles aswell as programs and fonts.  
If the programs installation will fail, the terminal will still be usable but with the normal commands.  
Afterwards you will need to change your terminal's font to one of the newly downloaded fonts.  
To be able to download and install all programs and fonts you **will need** priviledged access.

# Update

The repo update will be done automatically every week but you can also force it manually:

```
user@hostname:~$ cd .oh-my-zsh
user@hostname:~/.oh-my-zsh$ ./tools/upgrade.sh
```